import org.junit.Assert;
import org.junit.Test;

public class Test_Sorrow
{
    @Test
    public void test_sorrow()
    {
        String expected = "Late Night 3:30am";

        Assert.assertEquals(expected, Sorrow.getStatement());
    }
}
